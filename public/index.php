<?php
require __DIR__.'/../vendor/autoload.php';

use PokePHP\PokeApi;

$pokeApi = new PokeApi;
$pokemons = json_decode(
  $pokeApi->resourceList('pokemon')
);

?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <title>Pokédex</title>
</head>
<body>
  <div class="container">
    <h2 class="text-center">
      Pokemons
    </h2>

    <div class="row">
      <div class="col-md-12">
        <table class="table table-bordered table-hover table-striped">
          <tr>
            <th>Pokemon Name</th>
            <th></th>
          </tr>
          <?php foreach($pokemons->results as $pokemon): ?>
            <tr>
              <td>
                <?= $pokemon->name ?>
              </td>
              <td>
                <a href="#">View Details</a>
              </td>
            </tr>
          <?php endforeach ?>
        </table>
      </div>
    </div>
  </div>
</body>
</html>
